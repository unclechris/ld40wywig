using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButtonLoader : MonoBehaviour
{

	public GameObject levelButtonPrefab;
	public GameObject levelButtonContainer;
	public GameObject levelCanvas;
	public GameObject HUDCanvas;

	private List<GameObject> createdButtons = new List<GameObject>();
	// Use this for initialization
	void Start()
	{
		CreateLevelButtons();
	}

	public void CreateLevelButtons()
	{
		Sprite[] maps = Resources.LoadAll<Sprite>("Maps");
		int count = 0;
		foreach (GameObject go in createdButtons)
		{
			if (go != null)
			{
				DestroyImmediate(go);
			}
		}

		foreach (Sprite map in maps)
		{
			count++;
			GameObject newLevelButton = Instantiate(levelButtonPrefab) as GameObject;
			createdButtons.Add(newLevelButton);
			newLevelButton.transform.SetParent(levelButtonContainer.transform);
			Button LevelButton = newLevelButton.GetComponent<Button>();
			if (LevelButton != null)
			{
				if (GameManager.Instance.getPercentComplete(count) > 80)
				{
					GameManager.Instance.UnLockLevel(count + 1);
				}
				//if not 1 and locked, then reset image color and make non-interactable
				if (GameManager.Instance.isLevelLocked(count) == true && count > 1)
				{
					LevelButton.interactable = false;
					Color imageColor = LevelButton.image.color;
					imageColor.a = 0.5f;
					LevelButton.image.color = imageColor;
				}
				else
				{
				}
			}
			Text[] textChildren = newLevelButton.GetComponentsInChildren<Text>();
			foreach (Text textChild in textChildren)
			{
				if (textChild.name == "Level")
				{
					textChild.name = "Level_" + count.ToString("000");
					textChild.text = count.ToString();
					// Set click working for Menu
					UnityEngine.Events.UnityAction action = () => { GameManager.Instance.SelectLevel(System.Convert.ToInt32(textChild.text)); };
					LevelButton.onClick.AddListener(action);
					action = () => { levelCanvas.SetActive(false); };
					LevelButton.onClick.AddListener(action);
					action = () => { HUDCanvas.SetActive(true); };
					LevelButton.onClick.AddListener(action);

				}
				else
				if (textChild.name == "Percent")
				{
					if (GameManager.Instance.isLevelLocked(count) == false || GameManager.Instance.getPercentComplete(count) > 0)
					{
						textChild.text = GameManager.Instance.getPercentComplete(count).ToString();
					}
					else
					{
						textChild.text = "";
					}

				}
			}

		}

	}
}
