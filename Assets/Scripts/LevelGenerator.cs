using UnityEngine;
using System;


public class LevelGenerator : MonoBehaviour
{
	Texture2D map;
	public ColorToPrefab[] colorMappings;
	protected int myLevel;
	public static LevelGenerator Instance;

	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
	}
	// Update is called once per frame
	public void GenerateLevel()
	{
		myLevel = GameManager.Instance.CurrentLevel;
		map = Resources.Load<Sprite>("Maps/Level" + myLevel.ToString("00")).texture;

		Transform levelHolder = transform.Find("Level Holder");
		if (levelHolder != null)
		{
			DestroyImmediate(levelHolder.gameObject);
		}
		GameObject levelBase = new GameObject("Level Holder");
		levelBase.transform.SetParent(transform);
		for (int x = 0; x < map.width; x++)
		{

			{
				for (int y = 0; y < map.height; y++)
				{
					GenerateTile(x, y, levelBase.transform);
				}
			}
		}
	}

	private void GenerateTile(int x, int y, Transform parentTransform)
	{

		bool hasPlayerSpawned = false;
		Color pixColor = map.GetPixel(x, y);
		if (pixColor.a == 0)
		{
			return;
		}
		foreach (ColorToPrefab colorMapping in colorMappings)
		{
			if (colorMapping.color == pixColor)
			{
				Vector2 pos = new Vector2(x, y);
				if (colorMapping.prefab != null)
				{
					int selected = UnityEngine.Random.Range(0, colorMapping.prefab.Length);

					if (colorMapping.prefab[selected].tag != "Player" || hasPlayerSpawned == false)
					{
						GameObject go = Instantiate(colorMapping.prefab[selected], pos, Quaternion.identity, parentTransform);
						if (go.tag == "Player")
						{
							GameManager.Instance.SetCameraFollow(go);
						}
					}

				}
			}
		}
	}
}
