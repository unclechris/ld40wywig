using UnityEditor;
using UnityEngine;
using System.Collections;

[CustomEditor(typeof(LevelGenerator))]
public class LoadLevel : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if (GUILayout.Button("Load Level"))
		{
			LevelGenerator myLevel = (LevelGenerator)target;
			myLevel.GenerateLevel();
		}
	}

}
