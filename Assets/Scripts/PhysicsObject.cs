using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour {

	public float minGroundNormalY = .65f;
	public float gravityModifer;
	public float JumpHeight;
	public float JumpTime;

	protected Vector2 targetVelocity;
    protected bool grounded;
	protected Rigidbody2D rb2d;
	protected Vector2 velocity;
	protected RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
	protected List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);
	protected const float minMoveDistance = 0.001f;
	protected const float shellRadius = 0.01f;
	protected ContactFilter2D contactFilter;
	protected Vector2 groundNormal;

	private void OnEnable()
	{
		rb2d = GetComponent<Rigidbody2D>();
		RecalcGravityModifer(JumpTime, JumpHeight);
	}

	protected void RecalcGravityModifer(float newJumpTime, float newJumpHeight)
	{
		if (newJumpTime != 0.00 && newJumpHeight != 00)
		{
			gravityModifer = 8 * newJumpHeight / (newJumpTime * newJumpTime);
			gravityModifer--;
		}
		else
			gravityModifer = 1.0f;
		}

	// Use this for initialization
	void Start () {
		contactFilter.useTriggers = false;
		contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
		contactFilter.useLayerMask = true;
	}
	
	// Update is called once per frame
	void Update () {
		targetVelocity = Vector2.zero;
		ComputeVelocity();

	}
	protected virtual void ComputeVelocity()
	{

	}
	void FixedUpdate()
	{
		RecalcGravityModifer(JumpTime, JumpHeight);
		velocity += Vector2.up * gravityModifer * Time.deltaTime;
		velocity.x = targetVelocity.x;
			
		grounded = false;

		Vector2 deltaPosition = velocity * Time.deltaTime;

		Vector2 moveAlongGround = new Vector2(groundNormal.y, -groundNormal.x);

		Vector2 move = moveAlongGround * deltaPosition.x;

		Movement(move, false);

		move = Vector2.up *	deltaPosition.y;
		Movement(move,true);

	}

	void Movement(Vector2 move, bool yMovement)
	{
		float distance = move.magnitude;
		if (distance>minMoveDistance)
		{
			int count = rb2d.Cast(move, contactFilter, hitBuffer, distance + shellRadius);
			hitBufferList.Clear();
			for (int i = 0; i < count; i++)
			{
				hitBufferList.Add(hitBuffer[i]);

			}
			for (int i = 0; i < hitBufferList.Count; i++)
			{
				Vector2 currentNormal = hitBufferList[i].normal;
				if (currentNormal.y > minGroundNormalY)
				{
					grounded = true;
					if (yMovement)
					{
						groundNormal = currentNormal;
						currentNormal.x = 0;
					}
				}

				float projection = Vector2.Dot(velocity, currentNormal);
				if (projection<0)
				{
					velocity = velocity - projection * currentNormal;
				}

				float modifedDistance = hitBufferList[i].distance - shellRadius;
				distance = modifedDistance < distance ? modifedDistance : distance;
			}
		}

		rb2d.position = rb2d.position + move.normalized * distance;
	}
}
