using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	public int Gold;
	public int MaxGold;	
	public int CurrentHP;
	public int currentTutorial;
	public string[] tutorialSpeaking;
	public Text tutorialText;
	public int MaxHP;
	public int CurrentLevel;
	public bool GameRunning;
	private bool JustLoaded;
	public static GameManager Instance;

	public Text myText;
	public Text GoldText;
	public Text PercentText;
	public Text WinLoseText;

	public GameObject GameOverMenu;
	public GameObject myPlayer;

	public int GoldPercent;
	public Transform goldSlider;
	public Text goldPerText;
	public Transform hitPointSlider;
	public Text hitPerText;
	public int maxNumLevels;
	public Cinemachine.CinemachineVirtualCamera playerFollowCamera;

	// Use this for initialization
	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
		CurrentHP = MaxHP;
		GameRunning = false;
		UnLockLevel(1);
	}

	internal void DoDamage(int damage, HazardItem.TypeOfDamage type)
	{
		CurrentHP -= damage;
		CurrentHP = Mathf.Clamp(CurrentHP, 0, MaxHP);
		UpdateHPSlider();
		if (CurrentHP == 0)
		{
			GameOver();
		}
	}

	internal void AddGold(int value)
	{
		Gold += value;
		GoldPercent = Gold * 100 / MaxGold;
		UpdateGoldSlider();
		if (GoldPercent==100)
		{
			GameOver();
		}
	}


	internal void RemoveGold(int value)
	{
		Gold -= value;
		GoldPercent = Gold * 100 / MaxGold;
		UpdateGoldSlider();
		if (GoldPercent == 100)
		{
			GameOver();
		}
	}
	private void UpdateGoldSlider()
	{
		Vector3 curscale = goldSlider.transform.localScale;
		curscale.x = GoldPercent / 100.00f;
		goldPerText.text = GoldPercent.ToString() + "%";
		goldSlider.transform.localScale = curscale;
	}

	public void Update()
	{
		if (myText != null)
		{
			myText.text = "Gold: " + Gold.ToString();
		}
	}

	public void SetCameraFollow(GameObject go)
	{
		if (playerFollowCamera != null)
		{
			playerFollowCamera.Follow = go.transform;
			myPlayer = go;
		}

	}

	public void GameOver()
	{
		if (GameRunning && JustLoaded)
		{
			if (myPlayer != null)
			{
				Destroy(myPlayer);
			}
			//			AudioManager.Instance.Play("end game");
			string GoldRatioText = "You got " + Gold.ToString() + " gold\nout of " + MaxGold.ToString();
			GoldText.text = GoldRatioText;

			string GoldPercentText = GoldPercent.ToString() + "%";
			PercentText.text = GoldPercentText;
			if (GoldPercent > 80)
			{
				WinLoseText.text = "You Won";
				AudioManager.Instance.Play("win");
				UnLockLevel(CurrentLevel + 1);
			}
			else
			{
				WinLoseText.text = "You Lost!";
				AudioManager.Instance.Play("lose");
			}
			if (getPercentComplete(CurrentLevel) < GoldPercent)
			{
				setPercentComplete(CurrentLevel, GoldPercent);
				WinLoseText.text = WinLoseText.text + "\n New High!";
			}

			GameOverMenu.SetActive(true);
			GameRunning = false;
		}
	}

	public void SelectLevel(int Level)
	{
		CurrentLevel = Level;
		LevelGenerator.Instance.GenerateLevel();
		Gold = 0;
		UpdateGoldSlider();
		CurrentHP = MaxHP;
		UpdateHPSlider();
		UpdateHPSlider();
		JustLoaded = true;
		GameRunning = true;
	}

	private void UpdateHPSlider()
	{
		Vector3 curscale = hitPointSlider.transform.localScale;
		curscale.x = CurrentHP/ (MaxHP * 1.0f);
		int HPPercent = CurrentHP * 100 / MaxHP;
		hitPerText.text = HPPercent.ToString() + "%";
		hitPointSlider.transform.localScale = curscale;
	}

	public void QuitGame()
	{
		Application.Quit();
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#endif
	}

	public void ResetGame()
	{
		PlayerPrefs.DeleteAll();
		for (int i = 0; i < maxNumLevels; i++)
		{
			setPercentComplete(i+1, 0);
			LockLevel(i + 1);
		}
		UnLockLevel(1);
		AudioManager.Instance.SetMusicVol(AudioManager.Instance.musicVolume);
		AudioManager.Instance.SetOverallVol(AudioManager.Instance.overAllVolume);
	}
	internal bool isLevelLocked(int level)
	{
		string tempLevel = "Level_" + level.ToString("000");

		if (PlayerPrefs.HasKey("Lock" + tempLevel) == false)
		{
			if (level > 1)
			{
				LockLevel(level);
			}
			else
			{
				UnLockLevel(level);
			}
		}
		return PlayerPrefs.GetInt("Lock" + tempLevel) == 1 ? true : false;
	}

	internal int getPercentComplete(int level)
	{
		string tempLevel = "Level_" + level.ToString("000");
		if (PlayerPrefs.HasKey("Complete" + tempLevel) == false)
		{
			PlayerPrefs.SetInt("Complete" + tempLevel, 0);

		}
		return PlayerPrefs.GetInt("Complete" + tempLevel);
	}

	internal void setPercentComplete(int level, int percentComplete)
	{
		string tempLevel = "Level_" + level.ToString("000");
		PlayerPrefs.SetInt("Complete" + tempLevel, percentComplete);
	}

	internal void UnLockLevel(int level)
	{
		PlayerPrefs.SetInt("LockLevel_" + level.ToString("000"), 0);
	}

	internal void LockLevel(int level)
	{
		PlayerPrefs.SetInt("LockLevel_" + level.ToString("000"), 1);
	}

	public void ReloadScene()
	{
		SceneManager.LoadScene(0);
	}
	
	public void SpeakNextTutorial()
	{		
		if(currentTutorial<tutorialSpeaking.Length)
		{
			tutorialText.text = tutorialSpeaking[currentTutorial];
			currentTutorial++;
		}
	}
}
