using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTalky : MonoBehaviour {
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag=="Player")
		{
			AudioManager.Instance.Play("Tut"+GameManager.Instance.currentTutorial.ToString("0"));
			GameManager.Instance.SpeakNextTutorial();
			Destroy(gameObject);
		}
	}
}
