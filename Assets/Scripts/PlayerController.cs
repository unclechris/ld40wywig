using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : PhysicsObject
{
	public float jumpTakeOffSpeed = 7;
	public float jumpModifer;
	public float maxSpeed = 7;
	private SpriteRenderer sr;

	public bool needAppliedDown { get; private set; }

	// Use this for initialization
	void Awake()
	{
		sr = GetComponent<SpriteRenderer>();
	}

	// Update is called once per frame
	protected override void ComputeVelocity()
	{
		Vector2 move = Vector2.zero;

		move.x = Input.GetAxis("Horizontal");

		if (Input.GetButtonDown("Jump") && grounded)
		{
			velocity.y = jumpTakeOffSpeed - (GameManager.Instance.CurrentLevel * GameManager.Instance.GoldPercent/100.0f);
			AudioManager.Instance.Play("jump");
			needAppliedDown = true;
		}
		else
		{
			if (Input.GetButtonUp("Jump"))
			{
				if (velocity.y > 0)
				{
					velocity.y *= (jumpModifer - 1) * Time.deltaTime;
				}
			}
		}
		if (velocity.y<0 && needAppliedDown == true)
		{
			velocity.y *= (gravityModifer-1)*Time.deltaTime;
			needAppliedDown = false;
		}

		bool flipSprite = (sr.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
		if (flipSprite)
		{
			sr.flipX = !sr.flipX;

		}

		targetVelocity = move * maxSpeed;

	}

}
