using UnityEngine;
using System;

[Serializable]
public class ColorToPrefab
{
	public Color color;
	public GameObject[] prefab;

}
