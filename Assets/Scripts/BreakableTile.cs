using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableTile : MonoBehaviour
{

	public float timeTillDestruction;
	public bool canDestroy;
	public int supportWeight;
	private bool healing = false;
	public float destructionTime = 2.0f;
	public float healTime = 2.0f;
	public float startHoldingWeight;
	public float startHealingTime;
	public Sprite[] sprites;
	private SpriteMask mySM;
	protected int currentSprite;
	protected int lastSprite = -1;
	[SerializeField]
	protected bool touching =false;

	private void Start()
	{
		mySM = GetComponent<SpriteMask>();
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (healing == true)
		{
			healing = false;
		}
		//check if overweight
		if (GameManager.Instance.Gold > supportWeight)
		{
		Vector3 myVector = (transform.position - collision.gameObject.transform.position);
		Debug.DrawRay(transform.position, myVector.normalized * 5.0f);
		float vertDist = transform.position.y - collision.gameObject.transform.position.y;
		// Only touch the one in the cell below
		if (vertDist >= -1.006f && vertDist <= -1.00f)
		{
					startHoldingWeight = Time.time;
					touching = true;
			}
		}
	}

	private void OnCollisionStay2D(Collision2D collision)
	{
		if (GameManager.Instance.Gold > supportWeight)
		{
			if (canDestroy && startHoldingWeight != 0.00f )
			{
				Vector3 myVector = (transform.position - collision.gameObject.transform.position);
				Debug.DrawRay(transform.position, myVector.normalized * 5.0f);
				float vertDist = transform.position.y - collision.gameObject.transform.position.y;
				//float horizontalDist = (int)transform.position.x - (int)collision.gameObject.transform.position.x;
			//	Debug.Log("Vert dist:" + vertDist.ToString() + " Me:" + transform.position.y.ToString() + " Them:" + collision.gameObject.transform.position.y.ToString());

				if (vertDist >= -1.006f && vertDist <= -1.00f && touching)
				{
					timeTillDestruction = Time.time - startHoldingWeight;
					float adjustedructionTime = destructionTime * ((-.0075f * GameManager.Instance.GoldPercent)+1);
					if (timeTillDestruction > adjustedructionTime)
					{
						Destroy(gameObject);
						touching = false;
					}
				}
			}
		}
	}

	private void OnCollisionExit2D(Collision2D collision)
	{
		healing = true;
		startHealingTime = Time.time;
		touching = false;
	}

	private void Update()
	{
		if (GameManager.Instance.Gold > supportWeight)
		{
			if (startHoldingWeight == 0.0f)
			{
				startHoldingWeight = Time.time;
				healing = false;
			}
		}

		if (healing)
		{
			timeTillDestruction -= Time.deltaTime;
			if (timeTillDestruction < 0)
			{
				healing = false;
				startHoldingWeight = 0.0f;
			}
			else
			{
				startHoldingWeight = Time.time - timeTillDestruction;
			}
		}
		currentSprite = (int)((timeTillDestruction / destructionTime) * sprites.Length);
		if (currentSprite < sprites.Length && currentSprite >= 0)
		{
			if (currentSprite != lastSprite)
			{
				mySM.sprite = sprites[currentSprite];
				string soundClip = "";
				if (currentSprite > lastSprite)
				{
					soundClip = "breaking_" ;
				}
				else
				{
					soundClip = "healing_";
				}
				if (GameManager.Instance.GameRunning)
				{
					AudioManager.Instance.Play(soundClip + currentSprite.ToString("0"));
					lastSprite = currentSprite;
				}
			}
		}
	}

}
