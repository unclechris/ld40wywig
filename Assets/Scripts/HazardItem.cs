using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardItem : MonoBehaviour
{
	public enum TypeOfDamage { Fire, Poison, Slash, Blunt };

	public int damage;
	public bool continous;
	public float timeBetweenDealing;
	public float lastTimeTookDamage;
	public TypeOfDamage type;


	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			GameManager.Instance.DoDamage(damage, type);
			lastTimeTookDamage = Time.time;

		}
	}

	private void OnTriggerStay2D(Collider2D collision)
	{
		if (continous && collision.tag == "Player")
		{
			if (lastTimeTookDamage + timeBetweenDealing > Time.time)
			{
				GameManager.Instance.DoDamage(damage, type);
				lastTimeTookDamage = Time.time;
			}
		}
	}
}
