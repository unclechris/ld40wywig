using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestPickup : MonoBehaviour
{
	public int Value;
	public int MinValue;
	public int MaxValue;
	public int multiple;

	private void Awake()
	{
		if (MinValue!=MaxValue && MaxValue>MinValue)
		{
			Value = UnityEngine.Random.Range(MinValue, MaxValue);
			Value = (int)(Value / multiple)*multiple;
		}
		GameManager.Instance.MaxGold += Value;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			GameManager.Instance.AddGold(Value);
			AudioManager.Instance.Play("chest");
			Destroy(gameObject);
		}
	}
}
