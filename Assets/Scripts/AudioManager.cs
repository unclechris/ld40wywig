using UnityEngine.Audio;
using UnityEngine;
using UnityEngine.UI;
using System;

public class AudioManager : MonoBehaviour
{

	public static AudioManager Instance;
	[Range(0.0f, 1.0f)]
	public float overAllVolume;
	public Slider sliderAllVolume;
	[Range(0.0f, 1.0f)]
	public float musicVolume;
	public Slider sliderMusicVolume;

	public Sound[] Sounds;
	// Use this for initialization
	void Awake()
	{
		if (Instance == null)
			Instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
		InitSoundArray();
		if (PlayerPrefs.HasKey("OverAllSound"))
		{
			SetOverallVol(PlayerPrefs.GetFloat("OverAllSound"));
		}
		else
		{
			SetOverallVol(1.0f);
		}

		if (PlayerPrefs.HasKey("musicSound"))
		{
			SetMusicVol(PlayerPrefs.GetFloat("musicSound"));
		}
		else
		{
			SetMusicVol(1.0f);
		}
	}
	private void InitSoundArray()
	{
		foreach (Sound s in Sounds)
		{
			s.audioSource = gameObject.AddComponent<AudioSource>();
			s.audioSource.clip = s.clip;
			s.audioSource.volume = s.volume;
			s.audioSource.pitch = s.pitch;
			s.audioSource.loop = s.loop;
		}
	}

	public void Play(string name)
	{
		Sound s = Array.Find(Sounds, sound => sound.name == name);
		if (s != null)
		{
			if (name == "theme")
			{
				s.audioSource.volume = s.volume * musicVolume;
			}
			else
			{
				s.audioSource.volume = s.volume * overAllVolume;
			}
			s.audioSource.Play();
		}
	}
	public void SetOverallVol(float newValue)
	{
		string samplename = "win";
		overAllVolume = newValue;
		if (Sounds != null)
		{
			foreach (Sound s in Sounds)
			{
				if (s != null)
				{
					if (s.name != "theme")
					{
						if (s.audioSource != null)
						{
							s.audioSource.volume = s.volume * overAllVolume;
						}
					}
				}
			}

			Play(samplename);
			PlayerPrefs.SetFloat("OverAllSound", newValue);
		}
	}

	public void SetMusicVol(float newValue)
	{
		musicVolume = newValue;
		if (Sounds != null)
		{
			Sound s = Array.Find(Sounds, sound => sound.name == "theme");
			if (s != null)
			{
				if (s.audioSource != null)
				{
					s.audioSource.volume = s.volume * musicVolume;
				}
			}
			PlayerPrefs.SetFloat("musicSound", newValue);
		}
	}

	private void Start()
	{
		if (sliderAllVolume != null)
		{
			sliderAllVolume.value = overAllVolume;
		}
		if (sliderMusicVolume != null)
		{
			sliderMusicVolume.value = musicVolume;
		}
		Play("theme");
	}

}
