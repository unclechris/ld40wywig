using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStaysInPlayField : MonoBehaviour
{

	// Use this for initialization

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			GameManager.Instance.DoDamage(GameManager.Instance.CurrentHP * 2, HazardItem.TypeOfDamage.Slash);
		}
	}
}
